package com.demo.backend.plant;

import com.demo.backend.plant.dto.CreatePlantDto;
import com.demo.backend.plant.dto.UpdatePlantDto;
import com.demo.backend.plant.model.PlantView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/plants")
@RequiredArgsConstructor
@Validated
public class PlantController {

    private final PlantApplicationService applicationService;

    @GetMapping
    @Tag(name = "plants")
    @Operation(
            summary = "Find all plants",
            operationId = "getPlantList"
    )
    public List<PlantView> findAll() {
        return applicationService.findAll();
    }

    @GetMapping("{id}")
    @Tag(name = "plants")
    @Operation(
            summary = "Find a plant by id",
            operationId = "getPlantById"
    )
    public PlantView findById(@PathVariable UUID id) {
        return applicationService.get(id).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Tag(name = "plants")
    @Operation(
            summary = "Add plant",
            operationId = "addPlant"
    )
    public PlantView addPlant(@Valid @RequestBody CreatePlantDto createPlantDto) {
        return applicationService.addPlant(createPlantDto);
    }

    @PutMapping("{plantId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Tag(name = "plants")
    @Operation(
            summary = "Update plant",
            operationId = "updatePlant"
    )
    public UUID updatePlant(@Valid @RequestBody UpdatePlantDto updatePlantDto, @PathVariable UUID plantId) {
        PlantView result = applicationService.updatePlant(updatePlantDto, plantId);
        return result.getId();
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Tag(name = "plants")
    @Operation(
            summary = "Remove plant by ID",
            operationId = "removePlant"
    )
    public void remove(@PathVariable UUID id) {
        applicationService.delete(id);
    }
}
