package com.demo.backend.plant.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name="tbl_plant")
@Setter
@Getter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class PlantView {

    @Id
    @NonNull
    @ApiModelProperty(required = true)
    private UUID id;

    @NonNull
    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty(required = true)
    private boolean indoor;

    @ApiModelProperty(required = true)
    private String timesToWater;

}
