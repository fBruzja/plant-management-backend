package com.demo.backend.plant;

import com.demo.backend.plant.dto.CreatePlantDto;
import com.demo.backend.plant.dto.UpdatePlantDto;
import com.demo.backend.plant.model.PlantView;
import com.demo.backend.plant.model.PlantViewRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class PlantApplicationService {
    private final PlantViewRepository plantViewRepository;

    public PlantApplicationService(PlantViewRepository plantViewRepository) {
        this.plantViewRepository = plantViewRepository;
    }

    public List<PlantView> findAll() {
        return new ArrayList<>(plantViewRepository.findAll());
    }

    public Optional<PlantView> get(UUID id) {
        return plantViewRepository.findById(id);
    }

    public PlantView addPlant(CreatePlantDto createPlantDto) {
        PlantView plantView = new PlantView(
                UUID.randomUUID(),
                createPlantDto.getName(),
                createPlantDto.isIndoor(),
                createPlantDto.getTimesToWater()
        );
        plantViewRepository.save(plantView);
        return plantView;
    }

    public PlantView updatePlant(UpdatePlantDto updatePlantDto, UUID plantId) {
        PlantView plantView = plantViewRepository.findById(plantId).orElseThrow(EntityNotFoundException::new);

        plantView.setName(updatePlantDto.getName());
        plantView.setIndoor(updatePlantDto.isIndoor());
        plantView.setTimesToWater(updatePlantDto.getTimesToWater());

        return plantView;
    }

    public void delete(UUID id) {
        plantViewRepository.deleteById(id);
    }
}
