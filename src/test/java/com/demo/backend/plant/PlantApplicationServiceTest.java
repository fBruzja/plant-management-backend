package com.demo.backend.plant;

import com.demo.backend.plant.dto.CreatePlantDto;
import com.demo.backend.plant.dto.UpdatePlantDto;
import com.demo.backend.plant.model.PlantView;
import com.demo.backend.plant.model.PlantViewRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PlantApplicationServiceTest {
    private PlantApplicationService testee;

    @Mock
    PlantViewRepository repository;

    @BeforeEach
    void setUp() {
        testee = new PlantApplicationService(repository);
    }

    @Test
    void thatGetWillReturnCorrectPlant() {
        // given
        PlantView given = PlantMockFactory.get().build();
        UUID plantId = UUID.randomUUID();
        when(repository.findById(plantId)).thenReturn(Optional.of(given));

        // when
        PlantView actual = testee.get(plantId).orElse(new PlantView());

        // then
        assertThat(actual).isEqualTo(given);
    }

    @Test
    void thatFindAllWillReturnAllPlants() {
        // given
        List<PlantView> all = Collections.singletonList(PlantMockFactory.get().build());
        when(repository.findAll()).thenReturn(all);

        // when
        List<PlantView> actual = testee.findAll();

        // then
        assertThat(actual.get(0)).isEqualTo(all.get(0));
    }

    @Test
    void thatUpdatePlantWillSetCorrectPlantData() {
        // given
        UUID plantId = UUID.randomUUID();
        PlantView plant = PlantMockFactory.get().build();
        UpdatePlantDto dto = PlantMockFactory.getUpdatePlantDtoBuilder().build();
        when(repository.findById(plantId)).thenReturn(Optional.of(plant));

        // when
        testee.updatePlant(dto, plantId);

        // then
        assertThat(plant.getName()).isEqualTo(dto.getName());
        assertThat(plant.getTimesToWater()).isEqualTo(dto.getTimesToWater());
        assertThat(plant.isIndoor()).isEqualTo(dto.isIndoor());
    }

    @Test
    void thatAddPlantWillSavePlant() {
        // given
        CreatePlantDto dto = PlantMockFactory.getCreatePlantDtoBuilder().build();

        // when
        testee.addPlant(dto);

        // then
        verify(repository, times(1)).save(any(PlantView.class));
    }

    @Test
    void thatDeleteWillRemovePlant() {
        // given
        UUID plantId = UUID.randomUUID();

        // when
        testee.delete(plantId);

        // then
        verify(repository).deleteById(plantId);
    }
}