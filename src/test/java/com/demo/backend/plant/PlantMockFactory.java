package com.demo.backend.plant;

import com.demo.backend.plant.dto.CreatePlantDto;
import com.demo.backend.plant.dto.UpdatePlantDto;
import com.demo.backend.plant.model.PlantView;

import java.util.UUID;

public class PlantMockFactory {
    public static PlantView.PlantViewBuilder get() {
        UUID plantId = UUID.randomUUID();

        return PlantView.builder()
                .id(plantId)
                .indoor(false)
                .name("plant name")
                .timesToWater("2");
    }

    public static UpdatePlantDto.UpdatePlantDtoBuilder getUpdatePlantDtoBuilder() {
        return UpdatePlantDto.builder()
                .indoor(true)
                .name("plant name")
                .timesToWater("3");
    }

    public static CreatePlantDto.CreatePlantDtoBuilder getCreatePlantDtoBuilder() {
        return CreatePlantDto.builder()
                .indoor(false)
                .name("plant name")
                .timesToWater("3");
    }
}
