package com.demo.backend.plant.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PlantViewRepository extends JpaRepository<PlantView, UUID> {

}
